import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ToastOptions } from 'ionic-angular';
import { NetActivityPage } from '../net-activity/net-activity';
import { NetCheckPage } from '../net-check/net-check';
import { NetHealthPage } from '../net-health/net-health';


@IonicPage()
@Component({
    selector: 'page-troubleshoot',
    templateUrl: 'troubleshoot.html',
})

export class TroubleshootPage 
{
	toastOptions: ToastOptions;

  	constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController) 
  	{
  		this.toastOptions = 
  		{
  			message: 'ALERT: your website a.ca has changed by less than 20%. Please check the NET CHECK module etc...',
  			cssClass: 'toast',
  			dismissOnPageChange: true,
  			duration: 9000,
  			position: 'top'
  		}
    }

  	ngOnInit()
  	{
		setTimeout(() => this.triggerToast(), 1500);    	
  	}

    triggerToast()
    {
		this.toast.create(this.toastOptions).present();
    }

    openNetActivityPage()
    {
      this.navCtrl.push(NetActivityPage);
    }

    openNetCheckPage()
    {
      this.navCtrl.push(NetCheckPage);
    }    

  	openNetHealthPage()
  	{
  		this.navCtrl.push(NetHealthPage);
  	}

}
