import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-net-health',
  templateUrl: 'net-health.html',
})

export class NetHealthPage 
{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  	// Doughnut
	public doughnutChartLabels:string[] = ['busy', 'free'];
	public doughnutChartData:number[] = [82, 18];
	public doughnutChartType:string = 'doughnut';

	// events
	public chartClicked(e:any):void {
	  console.log(e);
	}

	public chartHovered(e:any):void {
	  console.log(e);
	}
}
