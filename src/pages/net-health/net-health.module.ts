import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NetHealthPage } from './net-health';

@NgModule({
  declarations: [
    NetHealthPage,
  ],
  imports: [
    IonicPageModule.forChild(NetHealthPage),
  ],
})
export class NetHealthPageModule {}
