import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NetCheckPage } from './net-check';

@NgModule({
  declarations: [
    NetCheckPage,
  ],
  imports: [
    IonicPageModule.forChild(NetCheckPage),
  ],
})
export class NetCheckPageModule {}
