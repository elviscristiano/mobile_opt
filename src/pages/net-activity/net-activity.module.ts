import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NetActivityPage } from './net-activity';

@NgModule({
  declarations: [
    NetActivityPage,
  ],
  imports: [
    IonicPageModule.forChild(NetActivityPage),
  ],
})
export class NetActivityPageModule {}
