import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiService } from '../../app/services/api.service';


@Component({
  	selector: 'page-api',
  	templateUrl: 'api.html'
})

export class ApiPage 
{

	items: any;
	category: any;

  	constructor(public navCtrl: NavController, private apiService: ApiService) 
	{
 		this.getDefaults();
  	}

  	ngOnInit()
  	{
    	this.getUsers(this.category);
  	}

	getDefaults()
	{
      	this.category = 'users';
	}	

  	getUsers(category)
  	{
  		this.apiService.getPosts(category)
  		.subscribe(response => {
  			this.items = response.data;
  		});
  	}

  	changeCategory()
  	{
    	this.getUsers(this.category);
  	}
}