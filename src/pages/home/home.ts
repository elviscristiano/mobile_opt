import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TroubleshootPage } from '../troubleshoot/troubleshoot';


@Component({
  	selector: 'page-home',
  	templateUrl: 'home.html'
})
export class HomePage 
{

  	constructor(public navCtrl: NavController) 
  	{
	}

  	openTroubleshootPage()
  	{
  		// pushes the page to the stack managed by navCtrl
  		this.navCtrl.push(TroubleshootPage);
  	}

}
