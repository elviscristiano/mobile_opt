import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';

@Injectable()

export class ApiService
{
	http: any;
	baseUrl: String;

	constructor(http: Http)
	{
		this.http = http;
		this.baseUrl = 'http://3d1256f5.ngrok.io/api'
	}

	getPosts(category)
	{
		return this.http.get(this.baseUrl + '/' + category)
		.map(res => res.json());
	}
}