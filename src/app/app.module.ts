import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';

import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { NetActivityPage } from '../pages/net-activity/net-activity';
import { NetCheckPage } from '../pages/net-check/net-check';
import { NetHealthPage } from '../pages/net-health/net-health';
import { TabsPage } from '../pages/tabs/tabs';
import { TroubleshootPage } from '../pages/troubleshoot/troubleshoot';

import { Badge } from '@ionic-native/badge';
import { ChartsModule } from 'ng2-charts';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { ApiService } from './services/api.service';


@NgModule({
  declarations: [
    MyApp,
    ContactPage,
    HomePage,
    NetActivityPage,
    NetCheckPage,
    NetHealthPage,
    TabsPage,
    TroubleshootPage
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    RoundProgressModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ContactPage,
    HomePage,
    NetActivityPage,
    NetCheckPage,
    NetHealthPage,
    TabsPage,
    TroubleshootPage  
  ],
  providers: [
    ApiService,
    Badge,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}